const Colors = {
  primary: '#6EA533',
  white: '#fff',
  grey: '#596c75',
  separator: '#f0f0f0',
  iron: '#D1D3D4',
  black: 'black',
};

export { Colors };
